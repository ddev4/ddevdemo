git_clone_url=0
branch_to_deploy=0
project_id=0
import_db=-1
import_file=-1
instance_id=0
admin_file_name=""
db_file_name=""
user_email=0
center_location_path="/home/testuser/uploaddata"
email_script_url=""
is_new=0
set -e

while :; do
     case ${1:-} in
         -i|--instance_id)
            shift
            instance_id=$1
            ;;
         -b|--branch)
            shift
            branch_to_deploy=$1
            ;;
         -n|--is_new)
            shift
            is_new=$1
            ;;
         -d|--import_db)
            shift
            import_db=$1
            ;;
         -f|--import_file)
            shift
            import_file=$1
             ;;
         -a|--admin_file)
            shift
            admin_file_name=$1
             ;;
         -s|--db_file_name)
            shift
            db_file_name=$1
             ;;
         -u|--user_email)
            shift
            user_email=$1
            ;;
         -g|--git_url)
            shift
            git_clone_url=$1
            ;;
         --)              # End of all options.
             shift
             break
             ;;
         -?*)
             printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
             ;;
         *)               # Default case: No more options, so break out of the loop.
             break
     esac

     shift
done
if [[ $instance_id == 0 ]]
then
    echo "InstanceID is required"
    #send email here
    exit 1
fi
execute_setup () {
    set -e
    cd ~/
    mkdir -p "projects"
    if [ -d "projects/${instance_id}" ]; then
        cd "projects/${instance_id}"
        git checkout $branch_to_deploy
        git pull origin $branch_to_deploy
        yes | ddev restart
    else
        if [[ $is_new == 1 ]]
        then
            mkdir -p "projects/${instance_id}"
            cd "projects/${instance_id}"
            #clone specifed branch
            git clone -b $branch_to_deploy $git_clone_url .
            mkdir local_asset
            echo $user_email >> local_asset/user_email.txt
            if [ -f ".ddev/config.local.yaml_" ]; then
                cp .ddev/config.local.yaml_ .ddev/config.local.yaml
            else
                touch .ddev/config.local.yaml
                echo "name: $instance_id" >> .ddev/config.local.yaml
                echo "additional_hostnames: " >> .ddev/config.local.yaml
                echo "  - $instance_id" >> .ddev/config.local.yaml
                #echo "additional_fqdns: " >> .ddev/config.local.yaml
                #echo "  - $instance_id.preview.internal.dev" >> .ddev/config.local.yaml
            fi
            echo "here.."

            mkdir .ddev/custom_certs
            #sudo cp /etc/ssl/internal/acmephp-internal-autodns/certs/wildcard.preview.internal.dev/private/key.private.pem .ddev/custom_certs/$instance_id.key
            #sudo cp /etc/ssl/internal/acmephp-internal-autodns/certs/wildcard.preview.internal.dev/public/cert.pem .ddev/custom_certs/$instance_id.key
            echo "before start.."
            yes | ddev start
            echo "sucessfully started"
        else
            echo "Instance does not exists"
            exit 1
        fi

    fi
    echo "before auth ssh complete"
    ddev auth ssh 2>&1 | tee output_ssh_auth
    echo "auth ssh complete"
    ddev composer install
    echo "composer install complete"
    ddev npm i npm@5
    echo "npm5 install  complete"
    ddev npm ci
    echo "npm ci is complete"
    #ddev npm i -g gulp-cli
    #ddev gulp build
    #./vendor/bin/typo3cms install:fixfolderstructure

    if [ $import_db == 1 ]
    then

        if [ ! -f "$center_location_path/$db_file_name" ]; then
            echo "DB File $center_location_path/$db_file_name does not exist."
            exit 1
        fi
        echo "Importing db"
        ddev import-db --file="$center_location_path/$db_file_name"
    fi

    if [ $import_file == 1 ]
    then

        if [ ! -f "$center_location_path/$db_file_name" ]; then
            echo "Admin File $center_location_path/$admin_file_name does not exist."
            exit 1
        fi
        echo "Importing file"
        ddev import-files --source="$center_location_path/$admin_file_name"
    fi
    echo "Exiting with success"
    exit 0  

}

send_mail() {
    curl -s -F attachment=@projects/${instance_id}_SETUP/last_pipeline_log \
    -F toemail=$user_email \
    -F subject=$1 \
    -F message=$2 \
    -F pipeline="ok" \
    $email_script_url
}

execute_setup 2>&1 | tee projects/${instance_id}_SETUP/last_pipeline_log
if [ ${PIPESTATUS[0]} -eq 0 ] 
then 
  echo "Successfully execution of pipeline"
  #send_mail "success" "${instance_id} is running"
  exit 0
else 
  echo "Pipeline failed with error"
  #send_mail "failed" "${instance_id} failed to deploy"
  exit 1
fi