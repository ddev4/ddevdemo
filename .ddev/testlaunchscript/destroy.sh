instance_id=0
set -e
while :; do
     case ${1:-} in

         -i|--instance_id)
            shift
            instance_id=$1
            ;;
         --)              # End of all options.
             shift
             break
             ;;
         -?*)
             printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
             ;;
         *)               # Default case: No more options, so break out of the loop.
             break
     esac

     shift
 done

if [[ $instance_id == 0 ]]
then
    echo "InstanceID is required"
    #send email here
    exit 1
fi
delete_instance (){
    cd projects/${instance_id}
    ddev stop --remove-data
    #ddev clean $instance_id
    #ddev delete $instance_id
    cd ..
    rm -rf $instance_id
}


send_mail() {
    curl -s -F attachment=@projects/${instance_id}_SETUP/last_destroy_pipeline_log \
    -F toemail=$user_email \
    -F subject=$1 \
    -F message=$2 \
    -F pipeline="ok" \
    $email_script_url
}

delete_instance 2>&1 | tee projects/${instance_id}_SETUP/last_destroy_pipeline_log
if [ ${PIPESTATUS[0]} -eq 0 ] 
then 
  echo "Successfully execution of pipeline"
  #send_mail "success" "${instance_id} is running"
  exit 0
else 
  echo "Pipeline failed with error"
  #send_mail "failed" "${instance_id} failed to deploy"
  exit 1
fi